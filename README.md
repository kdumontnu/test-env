# test env

A docker image for Gitea ci test

- From golang:1.18
- upgrade git to v2.36.0
- install git-lfs
- install [golangci-lint](https://github.com/golangci/golangci-lint) v1.45.2

# development environment build

In order to run from within a development environment at the root of the gitea repository with a command such as `docker run --volume $(pwd):/drone/src --workdir /drone/src --user gitea mytestenv ...` the id of the gitea user must match the id of the development environment.

- docker build --tag mytestenv --build-arg GITEA_ID=$(id -u) --build-arg GITEA_GID=$(id -g) .
