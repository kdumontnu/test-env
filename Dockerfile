FROM golang:1.18-bullseye
ARG GITEA_ID=1000
ARG GITEA_GID=1000

RUN addgroup \
    --gid $GITEA_GID \
    gitea && \
  adduser \
    --gecos '' \
    --shell /bin/bash \
    --uid $GITEA_ID \
    --gid $GITEA_GID \
    gitea

# upgrade git to v2.36.0
RUN curl -SL https://github.com/git/git/archive/v2.36.0.tar.gz \
    | tar -xzv -C /go \
    && apt-get update \
    && apt-get install -y libcurl4-gnutls-dev libexpat1-dev gettext libz-dev libssl-dev \
    && make -C /go/git-2.36.0 prefix=/usr all \
    && make -C /go/git-2.36.0 prefix=/usr install \
    && rm -rf /go/git-2.36.0 \
# install git-lfs
    && curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash \
    && apt-get install -y git-lfs \
# install golangci-lint
    && go install github.com/golangci/golangci-lint/cmd/golangci-lint@v1.45.2 \
    && golangci-lint --version
